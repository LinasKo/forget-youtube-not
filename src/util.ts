export class Counter {
  private count = 0;
  private limit: number;

  constructor(max = Infinity) {
    this.limit = max;
  }

  increment() {
    this.count++;
    console.debug("Counter:", this.count);
  }

  get() {
    return this.count;
  }

  overLimit() {
    return this.count > this.limit;
  }
}
