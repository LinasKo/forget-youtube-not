import { Env } from "./datatypes";
import YouTubeApi from "./youtubeApi";

/**
 * Welcome to Cloudflare Workers! This is your first scheduled worker.
 *
 * - Run `wrangler dev --local` in your terminal to start a development server
 * - Run `curl "http://localhost:8787/cdn-cgi/mf/scheduled"` to trigger the scheduled event
 * - Go back to the console to see what your worker has logged
 * - Update the Cron trigger in wrangler.toml (see https://developers.cloudflare.com/workers/wrangler/configuration/#triggers)
 * - Run `wrangler publish --name my-worker` to publish your worker
 *
 * Learn more at https://developers.cloudflare.com/workers/runtime-apis/scheduled-event/
 */

export default {
  // Run on the cron schedule
  async scheduled(
    controller: ScheduledController,
    env: Env,
    ctx: ExecutionContext
  ): Promise<void> {
    console.debug("Running scheduled event");
    const youtube = new YouTubeApi(env);
    await youtube.updateRecords();
  },

  // // Run on the website
  // async fetch(
  //   request: Request,
  //   env: Env,
  //   context: ExecutionContext
  // ): Promise<Response> {
  //   console.debug("Running fetch event");
  //   const youtube = new YouTubeApi(env);
  //   await youtube.updateRecords();
  //   return new Response(`YouTube records updated.`);
  // },
};
