export interface Env {
  DATABASE_URL: string;
  GOOGLE_API_KEY: string;
  YOUTUBE_CHANNEL_ID: string;
}

/** Parsed playlist data, received from YouTube API */
export interface YtPlaylist {
  id: string;
  title: string;
}

/** Parsed video data, received from YouTube API */
export interface YtVideo {
  id: string;
  title: string;
  privacyStatus: string;
  removed: boolean;
}
