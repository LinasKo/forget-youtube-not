import { PrismaClient, WorkerCache } from "@prisma/client/edge";

import { Env, YtPlaylist, YtVideo } from "./datatypes";
import { Counter } from "./util";

const CACHE_ID = "1"; // singleton in the database
const PRISMA_TRANSACTION_TIMEOUT_MS = 10000; // Default 5000

export default class Db {
  private prisma: PrismaClient;
  private subrequestCounter: Counter;
  private initialised = false;

  constructor(env: Env, subrequestCounter: Counter) {
    this.subrequestCounter = subrequestCounter;
    this.prisma = this.initPrisma(env);
  }

  async init(): Promise<WorkerCache> {
    const workerCache = await this.initCache();
    this.initialised = true;
    return workerCache;
  }

  async writeToCache(
    nextPlaylistId: string | null,
    nextVideoFetchToken: string | null
  ): Promise<void> {
    this.assertInitialised();

    console.debug("Writing to worker cache");
    this.subrequestCounter.increment();
    await this.prisma.workerCache.update({
      where: { id: CACHE_ID },
      data: {
        nextPlaylistId,
        nextVideoFetchToken,
      },
    });
    console.debug("Worker cache updated");
  }

  async updatePlaylistRecords(playlists: YtPlaylist[]): Promise<void> {
    this.subrequestCounter.increment();

    console.debug(`Updating ${playlists.length} playlists`);
    await this.prisma.$transaction(
      playlists.map((playlist) =>
        this.prisma.playlist.upsert({
          where: {
            id: playlist.id,
          },
          update: {
            title: playlist.title,
          },
          create: {
            id: playlist.id,
            title: playlist.title,
          },
        })
      )
    );
    console.debug(`Updated ${playlists.length} playlists`);
  }

  async updateVideoRecords(
    playlist: YtPlaylist | undefined,
    videos: YtVideo[]
  ): Promise<void> {
    const playlistTitle = playlist ? playlist.title : "<NONE>";
    console.debug(
      `Updating ${videos.length} videos in playlist '${playlistTitle}'`
    );
    this.subrequestCounter.increment();
    await this.prisma.$transaction(
      async (tx) => {
        videos.map(
          async (video) =>
            await tx.video.upsert({
              where: {
                id: video.id,
              },
              update: {
                title: video.title,
                playlistId: playlist?.id,
                privacyStatus: video.privacyStatus,
                removed: video.removed,
              },
              create: {
                id: video.id,
                title: video.title,
                originalTitle: video.title,
                url: `https://www.youtube.com/watch?v=${video.id}`,
                playlistId: playlist?.id,
                privacyStatus: video.privacyStatus,
                removed: video.removed,
              },
            })
        );
      },
      {
        timeout: PRISMA_TRANSACTION_TIMEOUT_MS,
      }
    );
    console.debug(
      `Updated ${videos.length} videos in playlist '${playlistTitle}'`
    );
  }

  private assertInitialised() {
    if (!this.initialised) {
      throw new Error("Db not initialised");
    }
  }

  private initPrisma(env: Env): PrismaClient {
    if (!env.DATABASE_URL) {
      throw new Error("DATABASE_URL is not set");
    }

    this.subrequestCounter.increment();
    return new PrismaClient({
      datasources: {
        db: {
          url: env.DATABASE_URL,
        },
      },
    });
  }

  /** Initialize cache holding worker metadata, to be read between runs */
  private async initCache(): Promise<WorkerCache> {
    this.subrequestCounter.increment();

    console.debug("Initializing worker cache");
    const workerCache = await this.prisma.workerCache.upsert({
      where: { id: CACHE_ID },
      update: {},
      create: {
        id: CACHE_ID,
        nextPlaylistId: null,
        nextVideoFetchToken: null,
      },
    });
    console.debug("Worker cache initialized");

    return workerCache;
  }
}
