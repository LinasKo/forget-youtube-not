import { Env, YtPlaylist, YtVideo } from "./datatypes";
import Db from "./db";
import { Counter } from "./util";

/*
The code runs in a Cloudflare worker. That imposes some limitations:
- The free version limits subrequests to 50.
- The subrequests seem to time out, but that has not been validated.
- The worker may consume up to 10ms of CPU time per request.

The full list of limitations can be found here: https://developers.cloudflare.com/workers/platform/limits
*/

// TODO: PRISMA HAS A maxWait on the transactions! Maybe that's what times out - not cloudflare! But that's only for interactive transactions. Not sure if normal ones have that.
// TODO: if so - write up!

const SUBREQUEST_SOFT_LIMIT = 20; // Cloudflare has a limit of 50 subrequests. Can't predict this accurately yet + some need to be saved for updating the cache. This also helps save CPU time.
console.assert(SUBREQUEST_SOFT_LIMIT <= 50, "SUBREQUEST_SOFT_LIMIT too high");

export default class YouTubeApi {
  private env: Env;
  private subrequestCounter: Counter;
  private db: Db;
  private initialised = false;
  private initialPlaylistId: string | null = null;
  private initialVideoFetchToken: string | null = null;

  constructor(env: Env) {
    this.assertEnv(env);
    this.env = env;
    this.subrequestCounter = new Counter(SUBREQUEST_SOFT_LIMIT);
    this.db = new Db(env, this.subrequestCounter);
  }

  /** The main function that updates the videos */
  async updateRecords() {
    if (!this.initialised) {
      await this.init();
      this.initialised = true;
    }

    // Date "YYYY-MM-DD HH:MM"
    const dateStr = new Date().toISOString().replace("T", " ").split(".")[0];
    console.info(`Starting video check at ${dateStr}`);

    const unsortedPlaylists = await this.fetchPlaylists();
    console.info(`Found ${unsortedPlaylists.length} playlists.`);
    if (unsortedPlaylists.length === 0) {
      console.info("========================================");
      return;
    }

    // Sort by ID
    const playlists = unsortedPlaylists.sort((a, b) => {
      if (a.id < b.id) {
        return -1;
      } else if (a.id > b.id) {
        return 1;
      } else {
        return 0;
      }
    });
    this.db.updatePlaylistRecords(playlists);

    // Maybe resume previous session - controlled by `playlistIndex` and `videoFetchToken`
    let videoFetchToken = this.initialVideoFetchToken;
    if (!this.initialPlaylistId) {
      console.error("videoFetchToken without playlistId");
      videoFetchToken = null;
    }

    let playlistIndex = playlists.findIndex(
      (p) => p.id === this.initialPlaylistId
    );
    if (playlistIndex === -1) {
      console.warn("Playlist not found, starting from the beginning");
      playlistIndex = 0;
      videoFetchToken = null;
    }

    // TODO: possible that token is invalid for this playlist. Maybe it can time out or gets invalidated when new videos are added.
    //       Observe and address if errors happen in production.

    while (
      playlistIndex < playlists.length &&
      !this.subrequestCounter.overLimit()
    ) {
      const playlist = playlists[playlistIndex];

      const [playlistItems, fetchToken] = await this.fetchVideos(
        playlist.id,
        videoFetchToken
      );
      const removedCount = playlistItems.filter((item) => item.removed).length;
      console.info(`* Found ${removedCount} removed in '${playlist.title}'`);

      // Update DB
      await this.db.updateVideoRecords(playlist, playlistItems);

      if (fetchToken) {
        // Continue fetching videos
        videoFetchToken = fetchToken;
      } else {
        // Advance to next playlist if no more videos in this one
        playlistIndex++;
        videoFetchToken = null;
      }
    }

    // Tell further workers where to resume
    if (playlistIndex < playlists.length) {
      const playlist = playlists[playlistIndex];
      this.db.writeToCache(playlist.id, videoFetchToken);
    } else {
      this.db.writeToCache(null, null);
      console.info("Finished updating all playlists");
    }
    console.info("========================================");
  }

  private async init() {
    const workerCache = await this.db.init();
    this.initialPlaylistId = workerCache.nextPlaylistId;
    this.initialVideoFetchToken = workerCache.nextVideoFetchToken;
  }

  private assertEnv(env: Env) {
    if (!env.GOOGLE_API_KEY) {
      throw new Error("No GOOGLE_API_KEY provided");
    }
    if (!env.YOUTUBE_CHANNEL_ID) {
      throw new Error("No YOUTUBE_CHANNEL_ID provided");
    }
    if (!env.DATABASE_URL) {
      throw new Error("No DATABASE_URL provided");
    }
  }

  /** Recursively fetch all playlist of a person.
   *
   * Does NOT account for subrequest soft limit - it is assumed the number of playlists is much less than (50 * STOP_AT_REQUEST_COUNT).
   */
  private async fetchPlaylists(): Promise<YtPlaylist[]> {
    let [playlists, fetchToken] = await this.fetchPlaylistsOnce(null);
    while (fetchToken) {
      const [morePlaylists, nextFetchToken] = await this.fetchPlaylistsOnce(
        fetchToken
      );
      playlists.push(...morePlaylists);
      fetchToken = nextFetchToken;
    }

    return playlists;
  }

  /** Fetch playlists once.
   * Returns [playlists, nextPageToken]
   */
  private async fetchPlaylistsOnce(
    fetchToken: string | null
  ): Promise<[YtPlaylist[], string]> {
    const URL = this.makePlaylistsUrl(fetchToken);

    this.subrequestCounter.increment();
    let json: any;
    try {
      const res = await fetch(URL, {
        headers: {
          Accept: "application/json",
        },
      });
      json = await res.json();
    } catch (err) {
      console.error("Error fetching playlists:", err);
      throw err;
    }

    const playlists: YtPlaylist[] = [];
    for (const item of json.items) {
      playlists.push({
        id: item.id,
        title: item.snippet.title,
      });
    }

    return [playlists, json.nextPageToken];
  }

  /** Fetch videos once.
   * Returns [videos, nextPageToken]
   */
  private async fetchVideosOnce(
    playlistId: string,
    fetchToken: string | null
  ): Promise<[YtVideo[], string]> {
    const URL = this.makeVideosUrl(playlistId, fetchToken);

    this.subrequestCounter.increment();
    let json: any;
    try {
      const res = await fetch(URL, {
        headers: {
          Accept: "application/json",
        },
      });
      json = await res.json();
    } catch (err) {
      console.error("Error fetching playlist items:", err);
      throw err;
    }

    const videos: YtVideo[] = [];
    for (const item of json.items) {
      const video: YtVideo = {
        id: item.snippet.resourceId.videoId,
        title: item.snippet.title,
        privacyStatus: item.status?.privacyStatus || "privacyStatusUnspecified",
        removed: false,
      };
      video.removed = this.isVideoRemoved(video);
      videos.push(video);
    }

    return [videos, json.nextPageToken];
  }

  /** Recursively fetch all videos of a playlist.
   * Accounts for the subrequest soft limit.
   */
  private async fetchVideos(
    playlistId: string,
    initialFetchToken: string | null
  ): Promise<[YtVideo[], string]> {
    let [videos, fetchToken] = await this.fetchVideosOnce(
      playlistId,
      initialFetchToken
    );

    while (fetchToken && !this.subrequestCounter.overLimit()) {
      const [moreVideos, nextFetchToken] = await this.fetchVideosOnce(
        playlistId,
        fetchToken
      );
      videos.push(...moreVideos);
      fetchToken = nextFetchToken;
    }

    return [videos, fetchToken];
  }

  /** Determine if the video is removed */
  private isVideoRemoved(video: any) {
    if (
      video.privacyStatus === "private" ||
      video.privacyStatus === "privacyStatusUnspecified"
    ) {
      return true;
    }

    if (video.title.startsWith("Deleted video")) {
      return true;
    }

    return false;
  }

  private makeUrl(
    urlBase: string,
    params: string[],
    keyValParams: Record<string, string>,
    pageToken?: string
  ): string {
    let url = urlBase;
    if (
      params.length > 0 ||
      Object.keys(keyValParams).length > 0 ||
      pageToken
    ) {
      url += "?";
    } else {
      return url;
    }

    let paramsAdded = false;
    if (params.length > 0) {
      url += "part=";
      url += params.join("%2C");
      paramsAdded = true;
    }

    let keyValParamsAdded = false;
    if (Object.keys(keyValParams).length > 0) {
      if (paramsAdded) {
        url += "&";
      }
      url += Object.entries(keyValParams)
        .map(([key, val]) => `${key}=${val}`)
        .join("&");
      keyValParamsAdded = true;
    }

    if (pageToken) {
      if (paramsAdded || keyValParamsAdded) {
        url += "&";
      }
      url += `pageToken=${pageToken}`;
    }

    return url;
  }

  private makeVideosUrl(playlistId: string, fetchToken: string | null): string {
    const MAX_RESULTS = 50; // Imposed by YouTube API

    return this.makeUrl(
      "https://youtube.googleapis.com/youtube/v3/playlistItems",
      ["snippet", "status"],
      {
        playlistId,
        maxResults: MAX_RESULTS.toString(),
        key: this.env.GOOGLE_API_KEY,
      },
      fetchToken || undefined
    );
  }

  private makePlaylistsUrl(fetchToken: string | null): string {
    const MAX_RESULTS = 50; // Imposed by YouTube API

    return this.makeUrl(
      "https://youtube.googleapis.com/youtube/v3/playlists",
      ["snippet", "id"],
      {
        channelId: this.env.YOUTUBE_CHANNEL_ID,
        maxResults: MAX_RESULTS.toString(),
        key: this.env.GOOGLE_API_KEY,
      },
      fetchToken || undefined
    );
  }
}
