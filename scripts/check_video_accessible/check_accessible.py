import csv
import os
from dotenv import load_dotenv
import requests
from prettyprinter import cpprint

load_dotenv()
assert os.getenv("YOUTUBE_API_KEY"), "YOUTUBE_API_KEY not set"


def read_csv(filename):
    """"Read an input CSV, with video IDs and titles"""
    with open(filename, "r") as f:
        reader = csv.reader(f)
        next(reader)
        return list(reader)


def check_video_accessible(video_id):
    """Check if a video is accessible"""
    url = f"https://www.googleapis.com/youtube/v3/videos?id={video_id}&key={os.getenv('YOUTUBE_API_KEY')}&part=snippet,status"
    response = requests.get(url)
    if response.status_code == 200:
        data = response.json()

        # Check if the video is accessible
        if not data["items"]:
            return False

    return True


def check_videos(vid_list):
    """Check if a list of videos are accessible"""
    for x in vid_list:
        try:
            video_id, title = x
        except Exception as e:
            print(f"Error: {e} for data {x}")

        if not check_video_accessible(video_id):
            print(f"* [ {video_id} ] {title} -> not accessible")


if __name__ == "__main__":
    videos = read_csv("input.csv")
    check_videos(videos)
