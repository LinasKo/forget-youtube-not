import os
from dotenv import load_dotenv
import requests
import argparse

load_dotenv()
assert os.getenv("YOUTUBE_API_KEY"), "YOUTUBE_API_KEY not set"

RED = "\033[31m"
YELLOW = "\033[33m"
PURPLE = "\033[35m"
RESET = "\033[0m"


def get_inaccessible_videos(playlist_id):
    """Get a list of inaccessible videos in a playlist"""

    all_items = []
    first_run = True
    next_token = None

    while first_run or next_token:
        first_run = False
        url = f"https://www.googleapis.com/youtube/v3/playlistItems?part=snippet%2Cstatus&key={os.getenv('YOUTUBE_API_KEY')}&playlistId={playlist_id}&maxResults=50"
        if next_token:
            url += f"&pageToken={next_token}"
        response = requests.get(url)
        if response.status_code != 200:
            print(f"Error getting playlist {playlist_id}")
            return []

        data = response.json()
        items = data["items"]
        all_items.extend(items)
        next_token = data.get("nextPageToken", None)

    inaccessible = []
    for item in all_items:
        video_id = item["snippet"]["resourceId"]["videoId"]
        title = item["snippet"]["title"]

        status = item["status"]["privacyStatus"]
        if title == "Deleted video":
            status = "deleted"
        if status not in ["public", "unlisted"]:
            inaccessible.append((video_id, title, status))

    return inaccessible


# def remove_from_playlist(playlist_id, video_id):
#     """Remove a video from a playlist"""
#     url = f"https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&key={os.getenv('YOUTUBE_API_KEY')}"
#     data = {
#         "snippet": {
#             "playlistId": playlist_id,
#             "resourceId": {
#                 "kind": "youtube#video",
#                 "videoId": video_id
#             }
#         }
#     }
#     response = requests.delete(url, json=data)
#     if response.status_code == 200:
#         data = response.json()
#         print(data)
#         print(f"Removed {video_id} from {playlist_id}")
#     else:
#         print(f"Error removing {video_id} from {playlist_id}:", response.text)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--playlist", help="Playlist ID to check")
    args = parser.parse_args()

    playlist_id = args.playlist
    inaccessible = get_inaccessible_videos(playlist_id)
    print(f"Found {len(inaccessible)} inaccessible videos in {playlist_id}:")
    for video_id, title, status in inaccessible:
        statusStr = status
        if status == "deleted":
            statusStr = f"{RED}D{RESET}"
        elif status == "private":
            statusStr = f"{YELLOW}P{RESET}"
        elif status not in ["unlisted", "public"]:
            statusStr = f"{PURPLE}?{RESET}"
        print(f"* {statusStr} <- [ {video_id} ] {title}")

    if not inaccessible:
        print("No inaccessible videos found")
        exit(0)

    # confirm = input("Remove all mentioned videos from playlist? [y/N] ")
    # if confirm.lower() != "y":
    #     print("Aborting")
    #     exit(0)

    print(
        f"OAuth Not implemented! Unfortunately, you'll need to remove these {len(inaccessible)} videos manually.")

    # for i, (video_id, title, status) in enumerate(inaccessible):
    #     remove_from_playlist(playlist_id, video_id)
    #     print(f"Removed {i+1}/{len(inaccessible)} videos", end="\r")

    # print(f"Done!")
