[![YouTube](https://img.shields.io/badge/YouTube-FF0000?style=for-the-badge&logo=YouTube&logoColor=white)](https://www.youtube.com/channel/UCZ9qFECXgT8Y8eYv9Z6uGyg)
![TypeScript](https://img.shields.io/badge/typescript-%23007ACC.svg?style=for-the-badge&logo=typescript&logoColor=white)
![Prisma](https://img.shields.io/badge/Prisma-3982CE?style=for-the-badge&logo=Prisma&logoColor=white)
![Cloudflare](https://img.shields.io/badge/Cloudflare-F38020?style=for-the-badge&logo=Cloudflare&logoColor=white)
![PlanetScale](https://img.shields.io/badge/PlanetScale-1A1A1A?style=for-the-badge&logo=PlanetScale&logoColor=white)


# Overview
Most of my music is on YouTube. But videos tend to disappear - be it because creators make them private, they get removed, or taken down. And that's fine. What I do mind is forgetting what they were. This project addresses that.

`forget-youtube-not` regularly scans your playlists, remembers their titles, and if the videos get removed - marks that in the database.

Note that safely storing the names and IDs in the database is the ultimate goal. It is intended for you to still do some work in crafting queries to see what exactly got removed.

# Complications
1. Cloudflare worker interactions with JS libraries are rather limited. For example, `PrismaClient` cannot be used from them. Instead, Prisma provides a data proxy as part of their `Prisma Data Platform`.
2. The free tier of Cloudflare workers only allows 50 subrequests. So I've built a mechanism for workers to pick up where others left off (via `WorkerCache`).
3. Updates take a long time. By default, Prisma times out after 5 seconds. Interactive transactions are used so that timeout duration can be increased.
4. Cloudflare only allows free workers to use 10ms of CPU time. Local, non-scheduled tests fail routinely. Cloudflare shows usage of 300ms+, (with 25 soft subrequest limit). If that persists, I expect to have major problems.

# Usage
The project is intended for personal use. The current setup relies on a hardcoded `YouTube` channel ID, as well as databases only I have access to. For now, no step-by-step implementation guide for replicating this setup exists.

## Deployment
1. Install the dependencies with `npm install`.
2. Log in to `wrangler` with `wrangler login`.
3. Check the cron configs in `wrangler.toml`.
4. Run `wrangler publish`
5. Check for errors on `https://dash.cloudflare.com/fc877c89f5284fbe8a9f36e0ff861143/workers/services/view/forget-youtube-not/production`

## Data
Data is on [Planetscale DB](https://planetscale.com/) so normally you'd use `prisma studio`. But because of the workers, indirection via [Prisma Data Platform](https://cloud.prisma.io/LinasKo/salmon-dog-qjfrbmvfdz/production/databrowser) introduced.

To check for inconsistencies, I need to look for the following patterns:
1. Video field `removed = true`
2. Video fields `originalTitle` and `title` don't match
3. ~~Video is not in a playlist~~ - removal not implemented after implementation of `WorkerCache` - playlists can be updated in part, by multiple workers.
4. Video field `updatedAt` is old.

# Project History & Motivation
For 5-10 years, I had this running on `PythonAnywhere`, written in Python. It would push data to a local `MySql` database. But I didn't like having to write SQL queries, and having used `Planetscale` + `Prisma` in another project, I liked browsing it with `prisma studio` better. Besides, it seemed like a good opportunity for getting better at cloud functions and finally try out `CloudFlare`.
